# Moneyhub Tech Test - Investments and Holdings

[Requirements](#requirements)

-   [Notes](#notes)

*   [Questions](#questions)

[Getting Started](#getting-started)

-   [Existing Routes](#existing-routes)

At Moneyhub we use microservices to partition and separate the concerns of the codebase. In this exercise we have given you an example `admin` service and some accompanying services to work with. In this case the admin service backs a front end admin tool allowing non-technical staff to interact with data.

A request for a new admin feature has been received

## <a id="requirements"></a>Requirements

-   An admin is able to generate a csv formatted report showing the values of all user holdings
    -   The report should be sent to the `/export` route of the investments service
    -   The investments service expects the report to be sent as csv text
    -   The csv should contain a row for each holding matching the following headers
        |User|First Name|Last Name|Date|Holding|Value|
    -   The holding should be the name of the holding account given by the financial-companies service
    -   The holding value can be calculated by `investmentTotal * investmentPercentage`
-   Ensure use of up to date packages and libraries (the service is known to use dmeprecated packages)
-   Make effective use of git

We prefer:

-   Functional code
-   Ramda.js (this is not a requirement but feel free to investigate)
-   Unit testing

### <a id="notes"></a>Notes

All of you work should take place inside the `admin` microservice

For the purposes of this task we would assume there are sufficient security middleware, permissions access and PII safe protocols, you do not need to add additional security measures as part of this exercise.

You are free to use any packages that would help with this task

We're interested in how you break down the work and build your solution in a clean, reusable and testable manner rather than seeing a perfect example, try to only spend around _1-2 hours_ working on it

### <a id="questions"></a>Questions

Relating to the task we'd also like you to write some answers to the following questions;

1. How might you make this service more secure?

    1. **Use secure channels such as SSL for receiving requests and sending response.**

    2. **Use authentication and authorization to ensure only users that are part of the system can access the APIs and users can only access resources that they are authorized to access.**

    3. **Add input validation to verify integrity of data amd ensure that the request does not contain malicious content.**

2. How would you make this solution scale to millions of records?

    1. **Use database for data storage.**

    2. **Writing optimized queries to ensure efficient data handling.**

    3. **Use pagination and next page tokens to split large requests into multiple chunks.**

    4. **Use caching (e.g. Redis) for frequently fetched data.**

3. What else would you have liked to improve given more time?

    1. **Break down the application into different modules and separate routes and controllers to have a lean code.**

    2. **Have separate utility modules which contain common/util modules such as json-2-csv, Ramda sorting method etc.**

    3. **Have separate services for communicating with financial-companies and investments servers.**

    4. **Add unit tests to ensure functionality is as per requirement.**

## <a id="getting-started"></a>Getting Started

Please clone this service and push it to your own github (or other) public repository

On completion email a link to your repository to your contact at Moneyhub and ensure it is publicly accessible.

To develop against all the services each one will need to be started in each service run

```bash
npm start
or
npm run develop
```

The develop command will run nodemon allowing you to make changes without restarting

The services will try to use ports 8081, 8082 and 8083

Use Postman or any API tool of you choice to trigger your endpoints (this is how we will test your new route). Please add your new routes to the readme

### <a id="existing-routes"></a>Existing routes

We have provided a series of routes

Investments - localhost:8081

-   `/investments` get all investments
-   `/investments/:id` get an investment record by id
-   `/investments/export` expects a csv formatted text input as the body

Financial Companies - localhost:8082

-   `/companies` get all companies details
-   `/companies/:id` get company by id

Admin - localhost:8083

-   `/investments/:id` get an investment record by id
