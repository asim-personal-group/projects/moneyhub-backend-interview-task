const express = require("express")
const config = require("config")
const axios = require("axios")
const R = require("ramda")
const converter = require("json-2-csv")

const createReportRowObject = ({userId, firstName, lastName, date, Holding, investmentTotal = 0, investmentPercentage = 0}) => {
  return {
    User: userId,
    "First Name": firstName,
    "Last Name": lastName,
    Date: date,
    Holding,
    Value: investmentTotal * investmentPercentage,
  }
}

const app = express()

app.use(express.json({limit: "10mb"}))
app.use(express.text())

app.get("/investments/:id", async (req, res) => {
  try {
    const {id} = req.params
    const investments = (await axios.get(`${config.investmentsServiceUrl}/investments/${id}`)).data
    return res.send(investments)
  } catch (err) {
    console.error(err)
    res.sendStatus(500)
  }
})

app.get("/generate-report", async (req, res) => {
  try {
    const sortByUserId = R.sortBy(R.prop("User"))
    const investments = (await axios.get(`${config.investmentsServiceUrl}/investments/`)).data
    const companies = (await axios.get(`${config.financialCompaniesServiceUrl}/companies`)).data
    const csvArray = []
    R.forEach((investment) => {
      const {userId, firstName, lastName, investmentTotal, date, holdings} = investment
      csvArray.push(
        ...R.map((holding) => {
          const {id: hid, investmentPercentage} = holding
          const Holding = R.find(R.propEq("id", hid))(companies).name
          return createReportRowObject({userId, firstName, lastName, date, Holding, investmentTotal, investmentPercentage})
        }, holdings)
      )
    }, investments)
    converter.json2csv(sortByUserId(csvArray), async (err, csv) => {
      if (err) {
        console.error(err)
        return res.sendStatus(500)
      }
      try {
        await axios.post(`${config.investmentsServiceUrl}/investments/export`, csv, {headers: {"Content-Type": "text/plain"}})
        return res.sendStatus(204)
      } catch (err) {
        console.error(err)
        return res.sendStatus(500)
      }
    })
  } catch (err) {
    console.error(err)
    res.sendStatus(500)
  }
})

app.listen(config.port, (err) => {
  if (err) {
    console.error("Error occurred starting the server", err)
    process.exit(1)
  }
  console.log(`Server running on port ${config.port}`)
})
